/*
    libmaus2
    Copyright (C) 2015 German Tischler

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#if ! defined(LIBMAUS2_DAZZLER_ALIGN_ALIGNMENTFILECONSTANTS_HPP)
#define LIBMAUS2_DAZZLER_ALIGN_ALIGNMENTFILECONSTANTS_HPP

#include <libmaus2/types/types.hpp>

namespace libmaus2
{
	namespace dazzler
	{
		namespace align
		{
			struct AlignmentFileConstants
			{
				static uint8_t const TRACE_XOVR;

				static int32_t getMinimumNonSmallTspace()
				{
					int32_t const val = TRACE_XOVR + 1;
					assert ( ! tspaceToSmall(val) );
					return val;
				}

				static bool tspaceToSmall(int64_t const tspace)
				{
					if ( tspace <= TRACE_XOVR )
						return true;
					else
						return false;
				}
			};
		}
	}
}
#endif
