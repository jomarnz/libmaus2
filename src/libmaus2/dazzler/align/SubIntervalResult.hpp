/*
    libmaus2
    Copyright (C) 2018 German Tischler-Höhle

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#if ! defined(LIBMAUS2_DAZZLER_ALIGN_SUBINTERVALRESULT_HPP)
#define LIBMAUS2_DAZZLER_ALIGN_SUBINTERVALRESULT_HPP

#include <libmaus2/types/types.hpp>

namespace libmaus2
{
	namespace dazzler
	{
		namespace align
		{
			struct SubIntervalResult
			{
				int64_t abpos;
				int64_t aepos;
				int64_t bbpos;
				int64_t bepos;

				SubIntervalResult() {}
				SubIntervalResult(
					int64_t const rabpos,
					int64_t const raepos,
					int64_t const rbbpos,
					int64_t const rbepos
				) : abpos(rabpos), aepos(raepos), bbpos(rbbpos), bepos(rbepos)
				{

				}
			};
		}
	}
}
#endif
