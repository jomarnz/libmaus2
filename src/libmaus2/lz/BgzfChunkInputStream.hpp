/*
    libmaus2
    Copyright (C) 2021 German Tischler

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#if ! defined(LIBMAUS2_LZ_BGZFCHUNKINPUTSTREAM_HPP)
#define LIBMAUS2_LZ_BGZFCHUNKINPUTSTREAM_HPP

#include <libmaus2/lz/BgzfChunkInputStreamBuffer.hpp>

namespace libmaus2
{
	namespace lz
	{
		struct BgzfChunkInputStream : public BgzfChunkInputStreamBuffer, public ::std::istream
		{
			BgzfChunkInputStream(std::istream & in, std::vector<libmaus2::lz::BgzfChunkInputStreamBuffer::Chunk> const & Vchunks)
			: BgzfChunkInputStreamBuffer(in,Vchunks), ::std::istream(this)
			{

			}
			BgzfChunkInputStream(std::shared_ptr<std::istream> rsIn, std::vector<libmaus2::lz::BgzfChunkInputStreamBuffer::Chunk> const & Vchunks)
			: BgzfChunkInputStreamBuffer(rsIn,Vchunks), ::std::istream(this)
			{

			}
		};
	}
}
#endif
