/*
    libmaus2
    Copyright (C) 2009-2015 German Tischler
    Copyright (C) 2011-2015 Genome Research Limited

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#if ! defined(LIBMAUS2_BAMBAM_BAMMULTIALIGNMENTDECODERFACTORY_HPP)
#define LIBMAUS2_BAMBAM_BAMMULTIALIGNMENTDECODERFACTORY_HPP

#include <libmaus2/bambam/BamAlignmentDecoderFactory.hpp>
#include <libmaus2/bambam/BamMergeCoordinate.hpp>
#include <libmaus2/bambam/BamCat.hpp>

namespace libmaus2
{
	namespace bambam
	{
		struct BamMultiAlignmentDecoderFactory
		{
			typedef BamAlignmentDecoderFactory this_type;
			typedef std::unique_ptr<this_type> unique_ptr_type;
			typedef std::shared_ptr<this_type> shared_ptr_type;

			std::vector<libmaus2::bambam::BamAlignmentDecoderInfo> const BADI;
			bool const putrank;

			BamMultiAlignmentDecoderFactory(std::vector<libmaus2::bambam::BamAlignmentDecoderInfo> const & rBADI, bool const rputrank = false) : BADI(rBADI), putrank(rputrank) {}
			virtual ~BamMultiAlignmentDecoderFactory() {}

			static std::set<std::string> getValidInputFormatsSet()
			{
				return libmaus2::bambam::BamAlignmentDecoderFactory::getValidInputFormatsSet();
			}

			static std::string getValidInputFormats()
			{
				return libmaus2::bambam::BamAlignmentDecoderFactory::getValidInputFormats();
			}

			libmaus2::bambam::BamAlignmentDecoderWrapper::unique_ptr_type operator()() const
			{
				libmaus2::bambam::BamAlignmentDecoderWrapper::unique_ptr_type tptr(construct(BADI,putrank));
				return tptr;
			}

			static libmaus2::bambam::BamAlignmentDecoderWrapper::unique_ptr_type construct(
				std::vector<libmaus2::bambam::BamAlignmentDecoderInfo> const & BADI,
				bool const putrank = false,
				std::istream & istdin = std::cin,
				bool cat = false,
				bool streaming = false
			)
			{
				if ( ! BADI.size() || BADI.size() > 1 )
				{
					if ( cat )
					{
						libmaus2::bambam::BamAlignmentDecoderWrapper::unique_ptr_type tptr(new libmaus2::bambam::BamCatWrapper(BADI,putrank,streaming));
						return tptr;
					}
					else
					{
						libmaus2::bambam::BamAlignmentDecoderWrapper::unique_ptr_type tptr(new libmaus2::bambam::BamMergeCoordinate(BADI,putrank));
						return tptr;
					}
				}
				else
				{
					libmaus2::bambam::BamAlignmentDecoderWrapper::unique_ptr_type tptr(
						libmaus2::bambam::BamAlignmentDecoderFactory::construct(BADI[0],putrank,istdin)
					);
					return tptr;
				}
			}

			static libmaus2::bambam::BamHeader::shared_ptr_type constructHeader(
				libmaus2::util::ArgInfo const & arginfo
			)
			{
				libmaus2::bambam::BamAlignmentDecoderWrapper::unique_ptr_type tptr(construct(arginfo));
				libmaus2::bambam::BamAlignmentDecoder & dec = tptr->getDecoder();
				libmaus2::bambam::BamHeader const & header = dec.getHeader();
				libmaus2::bambam::BamHeader::shared_ptr_type sheader(header.sclone());
				return sheader;
			}

			static libmaus2::bambam::BamHeader::shared_ptr_type constructHeader(
				libmaus2::util::ArgParser const & arg
			)
			{
				libmaus2::bambam::BamAlignmentDecoderWrapper::unique_ptr_type tptr(construct(arg));
				libmaus2::bambam::BamAlignmentDecoder & dec = tptr->getDecoder();
				libmaus2::bambam::BamHeader const & header = dec.getHeader();
				libmaus2::bambam::BamHeader::shared_ptr_type sheader(header.sclone());
				return sheader;
			}

			static libmaus2::bambam::BamHeader::shared_ptr_type constructHeader(
				std::string const & fn
			)
			{
				libmaus2::util::ArgInfo arginfo("progname");
				arginfo.replaceKey("I",fn);
				libmaus2::bambam::BamHeader::shared_ptr_type sheader(constructHeader(arginfo));
				return sheader;
			}

			static libmaus2::bambam::BamAlignmentDecoderWrapper::unique_ptr_type construct(
				libmaus2::util::ArgInfo const & arginfo,
				bool const putrank = false,
				std::ostream * copystr = 0,
				std::istream & istdin = std::cin,
				bool cat = false,
				bool streaming = false
			)
			{
				std::vector<std::string> const I = arginfo.getPairValues("I");

				std::vector<libmaus2::bambam::BamAlignmentDecoderInfo> V;
				for ( uint64_t i = 0; i < I.size(); ++i )
					V.push_back(libmaus2::bambam::BamAlignmentDecoderInfo::constructInfo(arginfo,I[i],false /* put rank */,copystr));
				if ( ! I.size() )
					V.push_back(libmaus2::bambam::BamAlignmentDecoderInfo::constructInfo(arginfo,"-",false,copystr));

				libmaus2::bambam::BamAlignmentDecoderWrapper::unique_ptr_type tptr(construct(V,putrank,istdin,cat,streaming));
				return tptr;
			}

			static libmaus2::bambam::BamAlignmentDecoderWrapper::unique_ptr_type construct(
				libmaus2::util::ArgParser const & arg,
				bool const putrank = false,
				std::ostream * copystr = 0,
				std::istream & istdin = std::cin,
				bool cat = false,
				bool streaming = false
			)
			{
				std::vector<std::string> const I = arg("I");

				std::vector<libmaus2::bambam::BamAlignmentDecoderInfo> V;
				for ( uint64_t i = 0; i < I.size(); ++i )
					V.push_back(libmaus2::bambam::BamAlignmentDecoderInfo::constructInfo(arg,I[i],false /* put rank */,copystr));
				if ( ! I.size() )
					V.push_back(libmaus2::bambam::BamAlignmentDecoderInfo::constructInfo(arg,"-",false,copystr));

				libmaus2::bambam::BamAlignmentDecoderWrapper::unique_ptr_type tptr(construct(V,putrank,istdin,cat,streaming));
				return tptr;
			}
		};
	}
}
#endif
