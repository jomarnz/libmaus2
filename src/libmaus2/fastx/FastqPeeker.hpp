/*
    libmaus2
    Copyright (C) 2016 German Tischler

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#if ! defined(LIBMAUS2_FASTX_FASTQPEEKER_HPP)
#define LIBMAUS2_FASTX_FASTQPEEKER_HPP

#include <libmaus2/fastx/FastQReader.hpp>
#include <libmaus2/fastx/StreamFastQReader.hpp>

namespace libmaus2
{
	namespace fastx
	{
		struct FastqPeeker
		{
			typedef FastqPeeker this_type;
			typedef std::unique_ptr<this_type> unique_ptr_type;
			typedef std::shared_ptr<this_type> shared_ptr_type;

			libmaus2::aio::InputStreamInstance::unique_ptr_type pISI;
			libmaus2::fastx::StreamFastQReaderWrapper dec;
			libmaus2::fastx::FastQReader::pattern_type slot;
			bool slotfilled;

			FastqPeeker(std::string const & fn)
			: pISI(new libmaus2::aio::InputStreamInstance(fn)), dec(*pISI), slot(), slotfilled(false) {}
			FastqPeeker(std::istream & in)
			: pISI(), dec(in), slot(), slotfilled(false) {}

			bool getNext(libmaus2::fastx::FastQReader::pattern_type & ralgn)
			{
				if ( slotfilled )
				{
					ralgn = slot;
					slotfilled = false;
					return true;
				}
				else
				{
					libmaus2::fastx::FastQReader::pattern_type pat;
					bool const ok = dec.getNextPatternUnlocked(pat);

					if ( ok )
					{
						ralgn = pat;
						return true;
					}
					else
					{
						return false;
					}
				}
			}

			bool peekNext(libmaus2::fastx::FastQReader::pattern_type & ralgn)
			{
				if ( ! slotfilled )
					slotfilled = getNext(slot);
				if ( slotfilled )
				{
					ralgn = slot;
					return true;
				}
				else
				{
					return false;
				}
			}
		};
	}
}
#endif
