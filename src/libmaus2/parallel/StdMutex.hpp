/*
    libmaus2
    Copyright (C) 2009-2013 German Tischler
    Copyright (C) 2011-2013 Genome Research Limited

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#if ! defined(LIBMAUS2_PARALLEL_STDMUTEX_HPP)
#define LIBMAUS2_PARALLEL_STDMUTEX_HPP

#include <libmaus2/LibMausConfig.hpp>
#include <libmaus2/exception/LibMausException.hpp>
#include <libmaus2/parallel/SimpleThreadPoolBase.hpp>
#include <cerrno>
#include <mutex>

namespace libmaus2
{
	namespace parallel
	{
		struct ScopeStdMutex;
                struct ScopeStdMutexTryLock;

                struct StdMutex
                {
                	typedef StdMutex this_type;
                	typedef std::unique_ptr<this_type> unique_ptr_type;
                	typedef std::shared_ptr<this_type> shared_ptr_type;

			typedef ScopeStdMutex scope_lock_type;
			typedef ScopeStdMutexTryLock scope_try_lock_type;

                	private:
                	StdMutex & operator=(StdMutex const & O) const;
                	StdMutex(StdMutex const & O);

                	public:
                	std::timed_mutex mutex;

                        StdMutex();
                        ~StdMutex() throw();
                        void lock();
                        void unlock();
                        bool trylock(uint64_t const seconds);
                        /**
                         * try to lock mutex. returns true if locking was succesful, false if mutex
                         * was already locked
                         **/
                        bool trylock();
                        /*
                         * try to lock mutex. if succesful, mutex is unlocked and return value is true,
                         * otherwise return value is false
                         */
                        bool tryLockUnlock();
                };

                struct ScopeStdMutex
                {
                	typedef ScopeStdMutex this_type;
                	typedef std::unique_ptr<this_type> unique_ptr_type;
                	typedef std::shared_ptr<this_type> shared_ptr_type;

			StdMutex & mutex;

			ScopeStdMutex(StdMutex & rmutex, bool const prelocked = false);
			~ScopeStdMutex();
			void lock();
			void unlock();
		};

                struct ScopeStdMutexTryLock
                {
                	typedef ScopeStdMutex this_type;
                	typedef std::unique_ptr<this_type> unique_ptr_type;
                	typedef std::shared_ptr<this_type> shared_ptr_type;

			StdMutex & mutex;
			bool locked;

			ScopeStdMutexTryLock(StdMutex & rmutex) : mutex(rmutex), locked(false)
			{
				locked = mutex.trylock();
			}
			~ScopeStdMutexTryLock()
			{
				if ( locked )
					mutex.unlock();
			}

			operator bool() const
			{
				return locked;
			}
		};
	}
}
#endif
