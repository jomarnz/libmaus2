/*
    libmaus2
    Copyright (C) 2009-2013 German Tischler
    Copyright (C) 2011-2013 Genome Research Limited

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#if ! defined(SYNCHRONOUSCOUNTER_HPP)
#define SYNCHRONOUSCOUNTER_HPP

#include <libmaus2/LibMausConfig.hpp>
#include <ostream>
#include <atomic>

namespace libmaus2
{
	namespace parallel
	{
		template<typename _value_type>
		struct SynchronousCounter
		{
			typedef _value_type value_type;
			typedef SynchronousCounter<value_type> this_type;

			std::atomic<value_type> cnt;

			SynchronousCounter(value_type const rcnt = value_type()) : cnt(rcnt) {}
			SynchronousCounter(SynchronousCounter const & o) : cnt(o.cnt.load()) {}
			SynchronousCounter & operator=(SynchronousCounter const & o)
			{
				if ( this != &o )
					this->cnt.store(o.cnt.load());
				return *this;
			}

			value_type operator++()
			{
				return ++cnt;
			}

			value_type operator+=(value_type const v)
			{
				return cnt += v;
			}

			value_type operator++(int)
			{
				return cnt++;
			}

			value_type get() const
			{
				return cnt;
			}

			operator value_type() const
			{
				return cnt;
			}
		};

		template<typename value_type>
		inline std::ostream & operator<<(std::ostream & out, SynchronousCounter<value_type> const & S)
		{
			out << S.get();
			return out;
		}
	}
}
#endif
