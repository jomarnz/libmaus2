/*
    libmaus2
    Copyright (C) 2019 German Tischler-Höhle

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#if ! defined(LIBMAUS2_MATH_DISTRIBUTION_HPP)
#define LIBMAUS2_MATH_DISTRIBUTION_HPP

#include <libmaus2/autoarray/AutoArray.hpp>

namespace libmaus2
{
	namespace math
	{
		template<typename _value_type>
		struct Distribution
		{
			typedef _value_type value_type;
			typedef Distribution<value_type> this_type;
			typedef std::unique_ptr<this_type> unique_ptr_type;
			typedef std::shared_ptr<this_type> shared_ptr_type;

			libmaus2::autoarray::AutoArray<value_type> A_X;
			uint64_t o_X;
			libmaus2::autoarray::AutoArray<double> A_Y;
			uint64_t o_Y;

			template<typename iterator>
			Distribution(iterator a, iterator e) : o_X(0), o_Y(0)
			{
				uint64_t c = 0;

				while ( a != e )
				{
					typename iterator::value_type const v = *a;

					while ( a != e && *a == v )
					{
						++a;
						++c;
					}

					A_X.push(o_X,v);
					A_Y.push(o_Y,c);
				}

				double const f = 1.0 / c;
				for ( uint64_t i = 0; i < o_Y; ++i )
					A_Y[i] *= f;
			}

			void reverse()
			{
				std::reverse(A_X.begin(),A_X.begin()+o_X);
				std::reverse(A_Y.begin(),A_Y.begin()+o_Y);
			}

			uint64_t size() const
			{
				return o_X;
			}
		};
	}
}
#endif
