/*
    libmaus2
    Copyright (C) 2019 German Tischler-Höhle

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#if ! defined(LIBMAUS2_MATH_NORMALDISTRIBUTION_HPP)
#define LIBMAUS2_MATH_NORMALDISTRIBUTION_HPP

#include <cmath>

namespace libmaus2
{
	namespace math
	{
		struct NormalDistribution
		{
			static double erf(double const x)
			{
				return ::std::erf(x);
			}

			// density of normal distribution
			static double density(double const x, double const mu, double const sigma)
			{
				return 1.0 / std::sqrt(2.0 * M_PI * sigma * sigma) * ::std::exp((-(x-mu)*(x-mu))/(2*sigma*sigma));
			}

			// distribution function of normal distribution
			static double cumulative(double const x, double const mu, double const sigma)
			{
				if ( x >= mu )
					return 0.5*(1.0+erf((x-mu)/(std::sqrt(2*sigma*sigma)) ));
				else
					return 1 - 0.5*(1.0+erf((mu-x)/(std::sqrt(2*sigma*sigma)) ));
			}
		};
	}
}
#endif
