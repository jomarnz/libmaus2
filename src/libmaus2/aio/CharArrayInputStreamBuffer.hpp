/*
    libmaus2
    Copyright (C) 2009-2014 German Tischler
    Copyright (C) 2011-2014 Genome Research Limited

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#if !defined(LIBMAUS2_AIO_CHARARRAYINPUTSTREAMBUFFER_HPP)
#define LIBMAUS2_AIO_CHARARRAYINPUTSTREAMBUFFER_HPP

#include <streambuf>
#include <istream>
#include <libmaus2/autoarray/AutoArray.hpp>

namespace libmaus2
{
	namespace aio
	{
		struct CharArrayInputStreamBuffer : public ::std::streambuf
		{
			public:
			typedef CharArrayInputStreamBuffer this_type;
			typedef std::unique_ptr<this_type> unique_ptr_type;
			typedef std::shared_ptr<this_type> shared_ptr_type;

			private:
			CharArrayInputStreamBuffer(CharArrayInputStreamBuffer const &);
			CharArrayInputStreamBuffer & operator=(CharArrayInputStreamBuffer &);

			public:
			CharArrayInputStreamBuffer(
				char * a,
				char * e
			)
			{
				setg(a,a,e);
			}

			private:
			// gptr as unsigned pointer
			uint8_t const * uptr() const
			{
				return reinterpret_cast<uint8_t const *>(gptr());
			}

			int_type underflow()
			{
				// if there is still data, then return it
				if ( gptr() < egptr() )
					return static_cast<int_type>(*uptr());

				assert ( gptr() == egptr() );

				return traits_type::eof();
			}
		};
	}
}
#endif
