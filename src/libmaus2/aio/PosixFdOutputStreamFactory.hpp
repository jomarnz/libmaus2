/*
    libmaus2
    Copyright (C) 2009-2015 German Tischler
    Copyright (C) 2011-2015 Genome Research Limited

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#if ! defined(LIBMAUS2_AIO_POSIXFDOUTPUTSTREAMFACTORY_HPP)
#define LIBMAUS2_AIO_POSIXFDOUTPUTSTREAMFACTORY_HPP

#include <libmaus2/aio/OutputStreamFactory.hpp>
#include <libmaus2/aio/PosixFdOutputStream.hpp>
#include <filesystem>

namespace libmaus2
{
	namespace aio
	{
		struct PosixFdOutputStreamFactory : public libmaus2::aio::OutputStreamFactory
		{
			typedef PosixFdOutputStreamFactory this_type;
			typedef std::unique_ptr<this_type> unique_ptr_type;
			typedef std::shared_ptr<this_type> shared_ptr_type;

			void copy(std::string const & from, std::string const & to);

			virtual ~PosixFdOutputStreamFactory() {}
			virtual libmaus2::aio::OutputStream::unique_ptr_type constructUnique(std::string const & filename)
			{
				if ( filename == "-" )
				{
					std::shared_ptr<std::ostream> iptr(new PosixFdOutputStream(STDOUT_FILENO));
					libmaus2::aio::OutputStream::unique_ptr_type istr(new libmaus2::aio::OutputStream(iptr));
					return istr;
				}
				else
				{
					std::shared_ptr<std::ostream> iptr(new PosixFdOutputStream(filename));
					libmaus2::aio::OutputStream::unique_ptr_type istr(new libmaus2::aio::OutputStream(iptr));
					return istr;
				}
			}
			virtual libmaus2::aio::OutputStream::shared_ptr_type constructShared(std::string const & filename)
			{
				if ( filename == "-" )
				{
					std::shared_ptr<std::ostream> iptr(new PosixFdOutputStream(STDOUT_FILENO));
					libmaus2::aio::OutputStream::shared_ptr_type istr(new libmaus2::aio::OutputStream(iptr));
					return istr;
				}
				else
				{
					std::shared_ptr<std::ostream> iptr(new PosixFdOutputStream(filename));
					libmaus2::aio::OutputStream::shared_ptr_type istr(new libmaus2::aio::OutputStream(iptr));
					return istr;
				}
			}
			virtual void rename(std::string const & from, std::string const & to)
			{
				if ( ::rename(from.c_str(),to.c_str()) == -1 )
				{
					int const error = errno;

					if ( error == EXDEV )
					{
						copy(from,to);
						::remove(from.c_str());
					}
					else
					{
						libmaus2::exception::LibMausException lme;
						lme.getStream() << "PosixFdOutputStreamFactory::rename(" << from << "," << to << "): " << strerror(error) << std::endl;
						lme.finish();
						throw lme;
					}
				}
			}
			virtual void mkdir(std::string const & name, uint64_t const mode)
			{
				std::filesystem::create_directory(name,std::filesystem::current_path());
				std::filesystem::permissions(name,static_cast<std::filesystem::perms>(mode));
			}
		};
	}
}
#endif
