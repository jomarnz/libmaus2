/*
    libmaus2
    Copyright (C) 2020 German Tischler-Höhle

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#if ! defined(LIBMAUS2_UTIL_STACKTRACEBUFFERCONTAINER_HPP)
#define LIBMAUS2_UTIL_STACKTRACEBUFFERCONTAINER_HPP

#include <libmaus2/LibMausConfig.hpp>

#include <csignal>
#include <iostream>
#include <thread>
#include <utility>
#include <atomic>
#include <setjmp.h>

namespace libmaus2
{
	namespace stacktrace
	{
		struct StackTraceBufferContainer
		{
			#define StackTraceBufferContainer_STACKTRACEBUFSIZE 30
			#define StackTraceBufferContainer_MAXTRACES 32

			typedef void * stacktracebuf_t[StackTraceBufferContainer_STACKTRACEBUFSIZE];

			struct StackTraceBuffer
			{
				stacktracebuf_t stacktracebuf;
				int stacktracebufn;

				StackTraceBuffer() : stacktracebufn(0) {}
			};

			typedef std::pair<std::thread::id,StackTraceBuffer> id_trace_type;
			typedef void (*libmaus2_sighandler_t)(int);

			static id_trace_type traces[StackTraceBufferContainer_MAXTRACES];
			static std::atomic<std::size_t> numtraces;

			static jmp_buf segenv;
			static libmaus2_sighandler_t prevsig;
			static int init;
			static int itranslate;

			static int setup();
			static void reset();
			static void sigseghandler(int);
		};
	}
}
#endif
