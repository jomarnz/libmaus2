/*
    Copyright (C) 2019-2020 German Tischler-Höhle

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#include <libmaus2/exception/LibMausException.hpp>

libmaus2::exception::LibMausException::LibMausException()
: postr(new std::ostringstream), s()
{

}
libmaus2::exception::LibMausException::LibMausException(std::string const & rs)
: postr(new std::ostringstream), s()
{
	getStream() << rs;
	finish();
}
libmaus2::exception::LibMausException::LibMausException(LibMausException const & LME)
: postr(LME.postr), s(LME.s) {}

libmaus2::exception::LibMausException::~LibMausException() throw()
{

}

libmaus2::exception::LibMausException::unique_ptr_type libmaus2::exception::LibMausException::uclone() const
{
        unique_ptr_type uptr(new this_type);

        std::shared_ptr<std::ostringstream> tptr(new std::ostringstream(postr->str()));
        uptr->postr = tptr;
        uptr->s = s;

        return uptr;
}

libmaus2::exception::LibMausException::unique_ptr_type libmaus2::exception::LibMausException::uclone(std::exception const & ex)
{
        unique_ptr_type Tptr(new this_type);
        Tptr->getStream() << ex.what() << std::endl;
        Tptr->finish();
        return Tptr;
}

std::ostream & libmaus2::exception::LibMausException::getStream()
{
        return *postr;
}

void libmaus2::exception::LibMausException::finish(bool translateStackTrace)
{
        s = postr->str();
        s += "\n";
        s += ::libmaus2::stacktrace::StackTrace::toString(translateStackTrace);
}

char const * libmaus2::exception::LibMausException::what() const throw()
{
        return s.c_str();
}
