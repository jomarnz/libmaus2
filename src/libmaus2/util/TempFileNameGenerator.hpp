/*
    libmaus2
    Copyright (C) 2009-2013 German Tischler
    Copyright (C) 2011-2013 Genome Research Limited

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#if ! defined(TEMPFILENAMEGENERATOR_HPP)
#define TEMPFILENAMEGENERATOR_HPP

#include <libmaus2/math/Log.hpp>
#include <libmaus2/exception/LibMausException.hpp>
#include <libmaus2/util/TempFileNameGeneratorState.hpp>
#include <libmaus2/util/TempFileRemovalContainer.hpp>
#include <vector>
#include <string>
#include <cassert>
#include <iomanip>
#include <algorithm>

#include <libmaus2/LibMausConfig.hpp>

namespace libmaus2
{
	namespace util
	{
		struct TempFileNameGenerator
		{
			typedef TempFileNameGenerator this_type;
			typedef std::unique_ptr<this_type> unique_ptr_type;

			::libmaus2::parallel::StdMutex lock;

			TempFileNameGeneratorState state;
			TempFileNameGeneratorState const startstate;

			TempFileNameGenerator(std::string const rprefix, unsigned int const rdepth, unsigned int const dirmod = TempFileNameGeneratorState::default_dirmod, unsigned int const filemod = TempFileNameGeneratorState::default_filemod);
			~TempFileNameGenerator();

			std::string getFileName(bool const regAsTempFile);
			std::string getFileName();
			void cleanupDirs();
		};
	}
}
#endif
