/*
    libmaus2
    Copyright (C) 2019 German Tischler-Höhle

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#if ! defined(LIBMAUS2_GTF_EXON_HPP)
#define LIBMAUS2_GTF_EXON_HPP

#include <libmaus2/math/IntegerInterval.hpp>
#include <libmaus2/util/NumberSerialisation.hpp>
#include <libmaus2/autoarray/AutoArray.hpp>

namespace libmaus2
{
	namespace gtf
	{
		struct Exon
		{
			uint64_t id;
			uint64_t id_offset;
			uint64_t start;
			uint64_t end;
			uint64_t strand;
			uint64_t transcript_id;
			uint64_t CDS_start;
			uint64_t CDS_end;

			bool operator==(Exon const & E) const
			{
				return
					id == E.id
					&&
					id_offset == E.id_offset
					&&
					start == E.start
					&&
					end == E.end
					&&
					strand == E.strand
					&&
					transcript_id == E.transcript_id
					&&
					CDS_start == E.CDS_start
					&&
					CDS_end == E.CDS_end
					;
			}

			bool operator!=(Exon const & E) const
			{
				return !operator==(E);
			}

			std::ostream & serialise(std::ostream & out) const
			{
				out.write(reinterpret_cast<char const *>(this),sizeof(Exon));
				return out;
			}

			std::istream & deserialise(std::istream & in)
			{
				in.read(reinterpret_cast<char *>(this),sizeof(Exon));
				return in;
			}

			Exon()
			{}

			Exon(
				uint64_t const rstart,
				uint64_t const rend
			) : start(rstart+1), end(rend) {}

			Exon(
				uint64_t const rid,
				uint64_t const rid_offset,
				uint64_t const rstart,
				uint64_t const rend,
				bool const rstrand,
				uint64_t const rtranscript_id,
				uint64_t const rCDS_start,
				uint64_t const rCDS_end
			) : id(rid), id_offset(rid_offset), start(rstart), end(rend), strand(rstrand), transcript_id(rtranscript_id), CDS_start(rCDS_start), CDS_end(rCDS_end)
			{}

			libmaus2::math::IntegerInterval<int64_t> getInterval() const
			{
				return libmaus2::math::IntegerInterval<int64_t>(getFrom(),getTo()-1);
			}

			std::pair<uint64_t,uint64_t> getPair() const
			{
				libmaus2::math::IntegerInterval<int64_t> const IA = getInterval();
				return std::pair<uint64_t,uint64_t>(IA.from,IA.to+1);
			}

			int64_t getFrom() const
			{
				return static_cast<int64_t>(start)-1;
			}

			int64_t getTo() const
			{
				return end;
			}

			bool operator<(Exon const & E) const
			{
				if ( start != E.start )
					return start < E.start;
				else
					return end > E.end;
			}

			std::ostream & printCoord(std::ostream & out) const
			{
				 out << "[" << getFrom() << "," << getTo() << ")";
				 return out;
			}

			std::ostream & print(
				std::ostream & out,
				libmaus2::autoarray::AutoArray<char> const & Aid,
				char const * chr,
				char const * gene_id,
				char const * gene_name,
				char const * transcript_id,
				uint64_t const exon_number
			) const
			{
				out
					<< chr
					<< "\t"
					<< "SOURCE"
					<< "\t"
					<< "exon"
					<< "\t"
					<< start
					<< "\t"
					<< end
					<< "\t"
					<< "."
					<< "\t"
					<< (strand?"+":"-")
					<< "\t"
					<< "."
					<< "\t"
					<< "gene_id \"" << gene_id << "\"; "
					<< "gene_name \"" << gene_name << "\"; "
					<< "transcript_id \"" << transcript_id << "\"; "
					<< "exon_id \"" << Aid.begin() + id_offset << "\";"
					<< "exon_number " << exon_number << ";"
					<< "\n";

				return out;
			}
		};

		std::ostream & operator<<(std::ostream & out, Exon const & E);
	}
}
#endif
