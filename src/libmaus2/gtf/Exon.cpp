/*
    libmaus2
    Copyright (C) 2019 German Tischler-Höhle

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#include <libmaus2/gtf/Exon.hpp>

std::ostream & libmaus2::gtf::operator<<(std::ostream & out, libmaus2::gtf::Exon const & E)
{
	out << "Exon(" <<
		"id=" << E.id << ","
		"id_offset=" << E.id_offset << ","
		"start=" << E.start << ","
		"end=" << E.end << ","
		"strand=" << E.strand << ","
		"transcript_id=" << E.transcript_id << ","
		"CDS_start=" << E.CDS_start << ","
		"CDS_end=" << E.CDS_end << ")";
	return out;
}
