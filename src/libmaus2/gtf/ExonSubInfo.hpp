/*
    libmaus2
    Copyright (C) 2019 German Tischler-Höhle

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#if ! defined(LIBMAUS2_GTF_EXONSUBINFO_HPP)
#define LIBMAUS2_GTF_EXONSUBINFO_HPP

#include <ostream>

namespace libmaus2
{
	namespace gtf
	{
		struct ExonSubInfo
		{
			uint64_t uncovered;
			uint64_t basecount;
			uint64_t total;
			uint64_t maxdepth;

			ExonSubInfo()
			: uncovered(0), basecount(0), total(0), maxdepth(0)
			{
			}

			ExonSubInfo & operator+=(ExonSubInfo const & E)
			{
				uncovered += E.uncovered;
				basecount += E.basecount;
				total += E.total;
				maxdepth = std::max(maxdepth,E.maxdepth);
				return *this;
			}

			ExonSubInfo operator+(ExonSubInfo const & E) const
			{
				ExonSubInfo T = *this;
				T += E;
				return T;
			}
			double getUncoveredFraction() const
			{
				if ( total )
				{
					return uncovered / static_cast<double>(total);
				}
				else
				{
					return 0;
				}
			}
		};

		std::ostream & operator<<(std::ostream & out, ExonSubInfo const & E);
	}
}
#endif
