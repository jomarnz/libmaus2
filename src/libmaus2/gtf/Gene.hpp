/*
    libmaus2
    Copyright (C) 2019 German Tischler-Höhle

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#if ! defined(LIBMAUS2_GTF_GENE_HPP)
#define LIBMAUS2_GTF_GENE_HPP

#include <libmaus2/util/NumberSerialisation.hpp>
#include <libmaus2/math/IntegerInterval.hpp>

namespace libmaus2
{
	namespace gtf
	{
		struct Gene
		{
			uint64_t chr_offset;
			uint64_t chr_id;
			uint64_t id_offset;
			uint64_t name_offset;
			uint64_t start;
			uint64_t end;
			uint64_t cstart;
			uint64_t cend;
			uint64_t id;
			uint64_t strand;

			std::string toString() const
			{
				std::ostringstream ostr;
				ostr
					<< "Gene("
					<< chr_offset << ","
					<< chr_id << ","
					<< id_offset << ","
					<< name_offset << ","
					<< start << ","
					<< end << ","
					<< cstart << ","
					<< cend << ","
					<< id << ","
					<< strand << ")";
				return ostr.str();
			}

			bool operator==(Gene const & O) const
			{
				bool const r
					=
					(
					(chr_offset == O.chr_offset)
					&&
					(chr_id == O.chr_id)
					&&
					(id_offset == O.id_offset)
					&&
					(name_offset == O.name_offset)
					&&
					(start == O.start)
					&&
					(end == O.end)
					&&
					(cstart == O.cstart)
					&&
					(cend == O.cend)
					&&
					(id == O.id)
					&&
					(strand == O.strand)
					);

				if ( ! r )
					std::cerr << toString() << "!=" << O.toString() << std::endl;

				return r;
			}

			bool operator!=(Gene const & O) const
			{
				return !operator==(O);
			}

			std::ostream & serialise(std::ostream & out) const
			{
				out.write(reinterpret_cast<char const *>(this),sizeof(Gene));
				return out;
			}

			std::istream & deserialise(std::istream & in)
			{
				in.read(reinterpret_cast<char *>(this),sizeof(Gene));
				return in;
			}

			Gene() {}
			Gene(uint64_t const rcstart, uint64_t const rcend)
			: cstart(rcstart), cend(rcend)
			{

			}
			Gene(
				uint64_t const rchr_offset,
				uint64_t const rid_offset,
				uint64_t const rname_offset,
				uint64_t const rstart,
				uint64_t const rend,
				uint64_t const rcstart,
				uint64_t const rcend,
				uint64_t const rid,
				bool const rstrand
			) : chr_offset(rchr_offset), chr_id(std::numeric_limits<uint64_t>::max()), id_offset(rid_offset), name_offset(rname_offset), start(rstart), end(rend), cstart(rcstart), cend(rcend), id(rid), strand(rstrand)
			{

			}

			int64_t getFrom() const
			{
				return static_cast<int64_t>(cstart)-1;
			}

			int64_t getTo() const
			{
				return static_cast<int64_t>(cend);
			}

			libmaus2::math::IntegerInterval<int64_t> getInterval() const
			{
				return libmaus2::math::IntegerInterval<int64_t>(getFrom(),getTo()-1);
			}

			std::ostream & print(std::ostream & out, libmaus2::autoarray::AutoArray<char> const & Aid) const
			{
				out
					<< Aid.begin() + chr_offset
					<< "\t"
					<< "SOURCE"
					<< "\t"
					<< "gene"
					<< "\t"
					<< cstart
					<< "\t"
					<< cend
					<< "\t"
					<< "."
					<< "\t"
					<< (strand?"+":"-")
					<< "\t"
					<< "."
					<< "\t"
					<< "gene_id \"" << Aid.begin()+id_offset << "\"; "
					<< "gene_name \"" << Aid.begin()+name_offset << "\";"
					<< "\n";

				return out;
			}
		};
	}
}
#endif
