/*
    libmaus2
    Copyright (C) 2019 German Tischler-Höhle

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#if ! defined(LIBMAUS2_GTF_TRANSCRIPT_HPP)
#define LIBMAUS2_GTF_TRANSCRIPT_HPP

#include <libmaus2/util/NumberSerialisation.hpp>

namespace libmaus2
{
	namespace gtf
	{
		struct Transcript
		{
			uint64_t id_offset;
			uint64_t start;
			uint64_t end;
			uint64_t strand;
			uint64_t gene_id;
			uint64_t c_start;
			uint64_t c_end;
			uint64_t id;
			uint64_t tag_start;
			uint64_t tag_end;
			int64_t support_level;

			bool operator==(Transcript const & T) const
			{
				return
					id_offset == T.id_offset
					&&
					start == T.start
					&&
					end == T.end
					&&
					strand == T.strand
					&&
					gene_id == T.gene_id
					&&
					c_start == T.c_start
					&&
					c_end == T.c_end
					&&
					id == T.id
					&&
					tag_start == T.tag_start
					&&
					tag_end == T.tag_end
					&&
					support_level == T.support_level
					;
			}

			bool operator!=(Transcript const & T) const
			{
				return !operator==(T);
			}

			Transcript() {}
			Transcript(uint64_t const rcstart, uint64_t const rcend)
			: c_start(rcstart), c_end(rcend) {}
			Transcript(
				uint64_t const rid_offset,
				uint64_t const rstart,
				uint64_t const rend,
				bool const rstrand,
				uint64_t const rgene_id,
				uint64_t const rc_start,
				uint64_t const rc_end,
				uint64_t const rid,
				uint64_t const rtagstart,
				uint64_t const rtagend,
				int64_t const rsupport_level
			) : id_offset(rid_offset), start(rstart), end(rend), strand(rstrand), gene_id(rgene_id),
			    c_start(rc_start), c_end(rc_end), id(rid), tag_start(rtagstart), tag_end(rtagend),
			    support_level(rsupport_level)
			{

			}

			int64_t getFrom() const
			{
				return static_cast<int64_t>(c_start)-1;
			}

			int64_t getTo() const
			{
				return c_end;
			}

			libmaus2::math::IntegerInterval<int64_t> getInterval() const
			{
				return libmaus2::math::IntegerInterval<int64_t>(getFrom(),getTo()-1);
			}

			std::ostream & serialise(std::ostream & out) const
			{
				out.write(reinterpret_cast<char const *>(this),sizeof(Transcript));
				return out;
			}

			std::istream & deserialise(std::istream & in)
			{
				in.read(reinterpret_cast<char *>(this),sizeof(Transcript));
				return in;
			}

			std::ostream & print(
				std::ostream & out,
				libmaus2::autoarray::AutoArray<char> const & Aid,
				char const * chr,
				char const * gene_id,
				char const * gene_name) const
			{
				out
					<< chr
					<< "\t"
					<< "SOURCE"
					<< "\t"
					<< "transcript"
					<< "\t"
					<< c_start
					<< "\t"
					<< c_end
					<< "\t"
					<< "."
					<< "\t"
					<< (strand?"+":"-")
					<< "\t"
					<< "."
					<< "\t"
					<< "transcript_id \"" << Aid.begin() + id_offset << "\"; "
					<< "gene_id \"" << gene_id << "\"; "
					<< "gene_name \"" << gene_name << "\";"
					<< "\n";

				return out;
			}
		};
	}
}
#endif
