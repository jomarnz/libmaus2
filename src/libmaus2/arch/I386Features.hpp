/*
    libmaus2
    Copyright (C) 2009-2020 German Tischler
    Copyright (C) 2011-2013 Genome Research Limited

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#if ! defined(I386FEATURES_HPP)
#define I386FEATURES_HPP

#include <libmaus2/LibMausConfig.hpp>
#include <iostream>
#include <utility>
#include <numeric>

namespace libmaus2
{
	namespace arch
	{
		/**
		 * class for i386 cache line size detection
		 **/
		struct I386Features
		{
			public:
			/**
			 * @return true if CPU supports SSE
			 **/
			static bool hasSSE();
			/**
			 * @return true if CPU supports SSE2
			 **/
			static bool hasSSE2();
			/**
			 * @return true if CPU supports SSE3
			 **/
			static bool hasSSE3();
			/**
			 * @return true if CPU supports SSSE3
			 **/
			static bool hasSSSE3();
			/**
			 * @return true if CPU supports SSE4.1
			 **/
			static bool hasSSE41();
			/**
			 * @return true if CPU supports SSE4.2
			 **/
			static bool hasSSE42();
			/**
			 * @return true if CPU supports popcnt
			 **/
			static bool hasPopCnt();
			/**
			 * @return true if CPU supports avx
			 **/
			static bool hasAVX();
			/**
			 * @return true if CPU supports avx
			 **/
			static bool hasAVX2();
			/**
			 * @return true if CPU supports pclmuldq
			 **/
			static bool hasPCLMULDQ();
			/**
			 * check whether CPU has a given feature from the list above
			 **/
			static bool hasFeature(char const * name);
		};
	}
}
#endif
