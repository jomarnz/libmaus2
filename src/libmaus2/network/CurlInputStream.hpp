/*
    libmaus2
    Copyright (C) 2020 German Tischler

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#if ! defined(LIBMAUS2_NETWORK_CURLINPUTSTREAM_HPP)
#define LIBMAUS2_NETWORK_CURLINPUTSTREAM_HPP

#include <libmaus2/network/CurlInputStreamBuffer.hpp>

namespace libmaus2
{
	namespace network
	{
		struct CurlInputStream : public CurlInputStreamBuffer, public std::istream
		{
			typedef CurlInputStream this_type;
			typedef std::unique_ptr<this_type> unique_ptr_type;
			typedef std::shared_ptr<this_type> shared_ptr_type;

			CurlInputStream(
				std::string const & rurl,
				uint64_t const bufsize = 16*1024,
				uint64_t const pushbacksize = 0
			);
		};
	}
}
#endif
