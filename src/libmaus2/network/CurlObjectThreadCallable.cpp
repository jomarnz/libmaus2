/*
    libmaus2
    Copyright (C) 2020 German Tischler-Höhle

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#include <libmaus2/network/CurlObjectThreadCallable.hpp>

libmaus2::network::CurlObjectThreadCallable::CurlObjectThreadCallable(libmaus2::network::CurlObject & rptr) : ptr(rptr), code(-1) {}
libmaus2::network::CurlObjectThreadCallable::~CurlObjectThreadCallable() {}

void libmaus2::network::CurlObjectThreadCallable::run()
{
	std::pair<int,std::string> const P = ptr.perform();
	code = P.first;
	errs = P.second;
	ptr.outputQueue.terminate();
}
int libmaus2::network::CurlObjectThreadCallable::getCode() const
{
	return static_cast<int>(code);
}
long libmaus2::network::CurlObjectThreadCallable::getFirstResponse() const
{
	return static_cast<long>(ptr.response);
}
long libmaus2::network::CurlObjectThreadCallable::getLastResponse() const
{
	return static_cast<long>(ptr.lastResponse);
}
std::string libmaus2::network::CurlObjectThreadCallable::getErrorString() const
{
	return errs;
}

std::string libmaus2::network::CurlObjectThreadCallable::getHeaderData() const
{
	return ptr.headerstr.str();
}
