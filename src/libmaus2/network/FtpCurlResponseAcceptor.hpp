/*
    libmaus2
    Copyright (C) 2020 German Tischler

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#if ! defined(LIBMAUS2_NETWORK_FTPCURLRESPONSEACCEPTOR_HPP)
#define LIBMAUS2_NETWORK_FTPCURLRESPONSEACCEPTOR_HPP

#include <libmaus2/network/CurlResponseAcceptor.hpp>

namespace libmaus2
{
	namespace network
	{
		struct FtpCurlResponseAcceptor : public CurlResponseAcceptor
		{
                        typedef FtpCurlResponseAcceptor this_type;
                        typedef std::unique_ptr<this_type> unique_ptr_type;
                        typedef std::shared_ptr<this_type> shared_ptr_type;

			virtual ~FtpCurlResponseAcceptor();
			virtual bool operator()(long const) const;
		};
	}
}
#endif
