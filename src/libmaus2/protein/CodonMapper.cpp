/*
    libmaus2
    Copyright (C) 2019 German Tischler-Höhle

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#include <libmaus2/protein/CodonMapper.hpp>

char const libmaus2::protein::CodonMapper::codonArray[64] = {
	// A
	'K','N','K','N',
	'T','T','T','T',
	'R','S','R','S',
	'I','I','M','I',
	// C
	'Q','H','Q','H',
	'P','P','P','P',
	'R','R','R','R',
	'L','L','L','L',
	// G
	'E','D','E','D',
	'A','A','A','A',
	'G','G','G','G',
	'V','V','V','V',
	// T/U
	'X','Y','X','Y',
	'S','S','S','S',
	'X','C','W','C',
	'L','F','L','F'
};

char const libmaus2::protein::CodonMapper::codonArrayMT[64] = {
	// A
	'K','N','K','N',
	'T','T','T','T',
	'X','S','X','S', // STOP AGA, AGG
	'M','I','M','I',
	// C
	'Q','H','Q','H',
	'P','P','P','P',
	'R','R','R','R',
	'L','L','L','L',
	// G
	'E','D','E','D',
	'A','A','A','A',
	'G','G','G','G',
	'V','V','V','V',
	// T
	'X','Y','X','Y', // STOP UAA,UAG
	'S','S','S','S',
	'W','C','W','C',
	'L','F','L','F'
};

char const libmaus2::protein::CodonMapper::stopArrayMT[64] = {
	// A
	0,0,0,0,
	0,0,0,0,
	1,0,1,0, // AGA, AGG
	0,0,0,0,
	// C
	0,0,0,0,
	0,0,0,0,
	0,0,0,0,
	0,0,0,0,
	// G
	0,0,0,0,
	0,0,0,0,
	0,0,0,0,
	0,0,0,0,
	// T/U
	1,0,1,0, // UAA,UAG
	0,0,0,0,
	0,0,0,0,
	0,0,0,0,
};


char const libmaus2::protein::CodonMapper::stopArray[64] = {
	// A
	0,0,0,0,
	0,0,0,0,
	0,0,0,0,
	0,0,0,0,
	// C
	0,0,0,0,
	0,0,0,0,
	0,0,0,0,
	0,0,0,0,
	// G
	0,0,0,0,
	0,0,0,0,
	0,0,0,0,
	0,0,0,0,
	// T/U
	1,0,1,0, // UAA,UAG
	0,0,0,0,
	1,0,0,0, // UGA
	0,0,0,0,
};

