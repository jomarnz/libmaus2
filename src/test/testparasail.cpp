/*
    libmaus2
    Copyright (C) 2021 German Tischler

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#include <libmaus2/protein/ProteinAlignment.hpp>
#include <cstring>
#include <libmaus2/bambam/CigarOperation.hpp>
#include <libmaus2/bambam/CigarStringParser.hpp>
#include <libmaus2/lcs/AlignmentPrint.hpp>
#include <libmaus2/util/ArgParser.hpp>

int main(
	[[maybe_unused]] int argc,
	[[maybe_unused]] char * argv[]
)
{
	try
	{
		#if defined(LIBMAUS2_HAVE_PARASAIL)
		libmaus2::protein::ProteinAlignment PA;

		std::string const sa("WWWWWWWWWWWWWWWSIABCTT");
		std::string const sb("SABC");

		char const * aa = sa.c_str();
		char const * ae = aa + sa.size();
		char const * ba = sb.c_str();
		char const * be = ba + sb.size();

		auto const R = PA.align(
			aa,ae,15,
			ba,be,0
		);

		std::cerr << R.toString() << std::endl;

		libmaus2::lcs::AlignmentPrint::printAlignmentLines(
			std::cerr,
			aa + R.abpos,
			R.aepos - R.abpos,
			ba + R.bbpos,
			R.bepos - R.bbpos,
			80, /* LW */
			PA.ta,
			PA.te,
			[](char const a){return a;},
			R.abpos,
			R.bbpos,
			"A",
			"B"
		);


		bool const ok = ( R == decltype(R)(16,20,0,4,15,"1X3=") );

		auto const M = R.enumerateMatches();

		for ( auto P : M )
			std::cerr << "(" << P.first << "," << P.second << ")" << std::endl;

		bool const m_ok =
			M == decltype(M)({
				std::make_pair(17,1),
				std::make_pair(18,2),
				std::make_pair(19,3)
			});

		return (ok && m_ok) ? EXIT_SUCCESS : EXIT_FAILURE;
		#else
		return EXIT_SUCCESS;
		#endif
	}
	catch(std::exception const & ex)
	{
		std::cerr << ex.what() << std::endl;
		return EXIT_FAILURE;
	}
}
