/*
    libmaus2
    Copyright (C) 2020 German Tischler

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#include <libmaus2/util/ArgParser.hpp>
#include <libmaus2/parallel/threadpool/ThreadPool.hpp>
#include <libmaus2/aio/StreamLock.hpp>

struct HelloWorldThreadPackageData : public libmaus2::parallel::threadpool::ThreadWorkPackageData
{
	std::string getText() const
	{
		return "Hello world";
	}
};

struct HelloWorldThreadPackageDispatcher : public libmaus2::parallel::threadpool::ThreadPoolDispatcher
{
	uint64_t const n;
	std::atomic<uint64_t> i;

	HelloWorldThreadPackageDispatcher(uint64_t const rn)
	: n(rn), i(0)
	{

	}

	virtual void dispatch(libmaus2::parallel::threadpool::ThreadWorkPackage & package, libmaus2::parallel::threadpool::ThreadPool * pool)
	{
		HelloWorldThreadPackageData & HP = dynamic_cast<HelloWorldThreadPackageData &>(*(package.data));
		uint64_t const li = ++i;

		{
			libmaus2::aio::StreamLock::lock_type::scope_lock_type slock(
				libmaus2::aio::StreamLock::cerrlock
			);
			std::cerr << "[" << li << "]\t" << HP.getText() << std::endl;
		}

		if ( li == n )
			pool->terminate();
	}
};

int main(int argc, char * argv[])
{
	try
	{
		libmaus2::util::ArgParser arg(argc,argv);

		libmaus2::parallel::threadpool::ThreadPool TP(12);
		uint64_t const n = 24;
		TP.registerDispatcher<HelloWorldThreadPackageData>(std::shared_ptr<libmaus2::parallel::threadpool::ThreadPoolDispatcher>(new HelloWorldThreadPackageDispatcher(n)));
		for ( uint64_t i = 0; i < n; ++i )
		{
			uint64_t const prio = i;
			std::pair<uint64_t,uint64_t> Pid(i,0);
			libmaus2::parallel::threadpool::ThreadWorkPackage::shared_ptr_type pack(TP.getPackage<HelloWorldThreadPackageData>(prio,Pid));
			TP.enqueue(pack);
		}

		TP.join();
	}
	catch(std::exception const & ex)
	{
		std::cerr << ex.what() << std::endl;
		return EXIT_FAILURE;
	}
}
