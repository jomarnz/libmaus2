/*
    libmaus2
    Copyright (C) 2017 German Tischler

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#include <libmaus2/util/ArgParser.hpp>
#include <libmaus2/util/ArgInfo.hpp>
#include <libmaus2/dazzler/align/LasSort2.hpp>

std::string getTmpFileBase(libmaus2::util::ArgParser const & arg)
{
	std::string const tmpfilebase = arg.uniqueArgPresent("T") ? arg["T"] : libmaus2::util::ArgInfo::getDefaultTmpFileName(arg.progname);
	return tmpfilebase;
}

int main(int argc, char * argv[])
{
	try
	{
		libmaus2::util::ArgParser const arg(argc,argv);

		if ( arg.size() < 1 )
		{
			libmaus2::exception::LibMausException lme;
			lme.getStream() << "[E] usage: " << argv[0] << " <out.las> <in.las> ..." << std::endl;
			lme.finish();
			throw lme;
		}

		std::string const outlas = arg[0];
		std::string const tmpbase = getTmpFileBase(arg);
		uint64_t const blocksize = arg.uniqueArgPresent("M") ? arg.getUnsignedNumericArg<uint64_t>("M") : (1024ull * 1024ull * 1024ull);
		uint64_t const fanin = 16;

		std::vector < std::string > Vin;
		for ( uint64_t z = 1; z < arg.size(); ++z )
			Vin.push_back(arg[z]);

		libmaus2::dazzler::align::LasSort2<>::lassort2(outlas,Vin,blocksize,fanin,tmpbase,1/* index */,&std::cerr);

		return EXIT_SUCCESS;
	}
	catch(std::exception const & ex)
	{
		std::cerr << ex.what() << std::endl;
		return EXIT_FAILURE;
	}
}
