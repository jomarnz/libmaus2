/*
    libmaus2
    Copyright (C) 2020 German Tischler-Höhle

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#include <libmaus2/parallel/threadpool/bgzf/BGZFInputThread.hpp>
#include <libmaus2/util/ArgParser.hpp>
#include <libmaus2/aio/StreamLock.hpp>

int main(int argc, char * argv[])
{
	try
	{
		libmaus2::util::ArgParser arg(argc,argv);

		std::vector<std::string> Vfn;
		for ( uint64_t i = 0; i < arg.size(); ++i )
			Vfn.push_back(arg[i]);

		if ( ! Vfn.size() )
		{
			std::cerr << "[E] need at least one input file name" << std::endl;
			return EXIT_FAILURE;
		}

		libmaus2::parallel::threadpool::ThreadPool TP(12);
		libmaus2::parallel::threadpool::bgzf::ThreadPoolBGZFReadControl TPBRC(TP,Vfn);
		libmaus2::parallel::threadpool::bgzf::BgzfBlockInfoTrivialReturnHandler BBITRH(TP,TPBRC.BITC);
		TPBRC.setBlockHandler(&BBITRH);
		TPBRC.start();

		TP.join();

		assert ( TPBRC.isIdle() );

		std::cerr << "[V] finished and joined" << std::endl;
	}
	catch(std::exception const & ex)
	{
		std::cerr << ex.what() << std::endl;
		return EXIT_FAILURE;
	}
}
