/*
    libmaus2
    Copyright (C) 2020 German Tischler

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#include <libmaus2/util/ArgParser.hpp>
#include <libmaus2/network/CurlInputStream.hpp>

int main(int argc, char * argv[])
{
	try
	{
		libmaus2::util::ArgParser arg(argc,argv);

		for ( uint64_t i = 0; i < arg.size(); ++i )
		{
			try
			{
				std::string const url = arg[i];
				libmaus2::network::CurlInputStream CIS(arg[i]);

				libmaus2::autoarray::AutoArray<char> B(8192);

				uint64_t s = 0;

				while ( CIS )
				{
					CIS.read(B.begin(),B.size());

					::std::size_t const n = CIS.gcount();

					std::cout.write(B.begin(),n);

					s += n;
				}

				std::cerr << "[V] downloaded file of size " << s << " from " << url << std::endl;
			}
			catch(std::exception const & ex)
			{
				std::cerr << ex.what() << std::endl;
			}
		}

		return 0;
	}
	catch(std::exception const & ex)
	{
		std::cerr << ex.what() << std::endl;
		return EXIT_FAILURE;
	}
}
