/*
    libmaus2
    Copyright (C) 2020 German Tischler-Höhle

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#include <libmaus2/parallel/threadpool/bgzf/DecompressThread.hpp>

namespace libmaus2
{
	namespace parallel
	{
		namespace threadpool
		{
			namespace bgzf
			{
				struct BgzfDataDecompressedTrivialReturnInterface : public BgzfDataDecompressedInterface
				{
					virtual void dataDecompressed(
						libmaus2::parallel::threadpool::input::ThreadPoolInputInfo * inputinfo,
						BgzfBlockInfo::shared_ptr_type blockinfo,
						uint64_t const blockid,
						uint64_t const subid,
						uint64_t const absid,
						std::shared_ptr<char[]> ddata,
						uint64_t const n,
						BgzfBlockInfoEnqueDecompressHandler * decompressHandler,
						BgzfBlockInfoProcessor * /* processor */
					)
					{
						uint64_t const streamid = inputinfo->streamid;

						BgzfBlockInfoEnqueDecompressHandler::OutputQueueInfo OQI = decompressHandler->getOutputQueue(streamid);

						OQI.push(BgzfDecompressedBlock(blockid,subid,absid,ddata,n,blockinfo));

						BgzfBlockInfoEnqueDecompressHandler::DecompressWaitingObject & decwa =
							decompressHandler->getDecompressWaiting(streamid);

						BgzfDecompressedBlock block;
						while ( OQI.pop(block) )
						{
							decompressHandler->returnBlock(streamid,block.ddata);

							bool const blockcomplete = decwa.incrementDone(block.blockid);
							//if ( blockcomplete )
							{
								decompressHandler->TPBRC.BITC.putInfo(
									streamid,block.blockinfo
								);
							}

							OQI.next();
						}

						uint64_t const lfinished = ++(decompressHandler->getAbsBlockFinished(streamid));

						if (
							decompressHandler->TPBRC.BITC.parseFinished(streamid)
							&&
							lfinished == decompressHandler->getAbsBlock(inputinfo->streamid).load()
						)
						{
							std::cerr << "stream decomp finished " << streamid << std::endl;

							uint64_t const nfinished = ++decompressHandler->numStreamsDecompressFinished;

							if ( nfinished == decompressHandler->numstreams )
							{
								decompressHandler->TP.terminate();
							}
						}
					}
				};

			}
		}
	}
}

#include <libmaus2/util/ArgParser.hpp>
#include <libmaus2/aio/StreamLock.hpp>

int main(int argc, char * argv[])
{
	try
	{
		libmaus2::util::ArgParser arg(argc,argv);

		std::vector<std::string> Vfn;
		for ( uint64_t i = 0; i < arg.size(); ++i )
			Vfn.push_back(arg[i]);

		if ( ! Vfn.size() )
		{
			std::cerr << "[E] need at least one input file name" << std::endl;
			return EXIT_FAILURE;
		}

		libmaus2::parallel::threadpool::ThreadPool TP(12);

		libmaus2::parallel::threadpool::bgzf::ThreadPoolBGZFDecompressControl TPBDC(TP,Vfn);

		libmaus2::parallel::threadpool::bgzf::BgzfDataDecompressedTrivialReturnInterface BDDTRH;
		TPBDC.setDecompressedHandler(&BDDTRH);

		TPBDC.start();

		TP.join();

		assert ( TPBDC.TPBRC.isIdle() );

		std::cerr << "[V] finished and joined" << std::endl;
	}
	catch(std::exception const & ex)
	{
		std::cerr << ex.what() << std::endl;
		return EXIT_FAILURE;
	}
}
