/*
    libmaus2
    Copyright (C) 2020 German Tischler

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#include <libmaus2/parallel/threadpool/input/ThreadPoolInput.hpp>
#include <libmaus2/aio/StreamLock.hpp>
#include <libmaus2/util/ArgParser.hpp>

int main(int argc, char * argv[])
{
	try
	{
		libmaus2::util::ArgParser arg(argc,argv);

		std::vector<std::string> Vfn;
		for ( uint64_t i = 0; i < arg.size(); ++i )
			Vfn.push_back(arg[i]);

		if ( ! Vfn.size() )
		{
			std::cerr << "[E] need at least one input file name" << std::endl;
			return EXIT_FAILURE;
		}

		libmaus2::parallel::threadpool::ThreadPool TP(12);
		uint64_t const baseprio = 64*1024;
		uint64_t const modprio = 256;
		libmaus2::parallel::threadpool::input::InputThreadControl IC(TP,Vfn,64,256*1024,1024,baseprio,modprio);
		libmaus2::parallel::threadpool::input::InputThreadTrivialReturnInterface<libmaus2::aio::StreamLock::lock_type> ITTRI(TP,IC,libmaus2::aio::StreamLock::cerrlock,std::cerr);
		IC.setCallback(std::vector<libmaus2::parallel::threadpool::input::InputThreadCallbackInterface *>(Vfn.size(),&ITTRI));
		IC.start();

		TP.join();
	}
	catch(std::exception const & ex)
	{
		std::cerr << ex.what() << std::endl;
		return EXIT_FAILURE;
	}
}
