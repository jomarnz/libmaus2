/*
    libmaus2
    Copyright (C) 2015 German Tischler

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#include <libmaus2/protein/AlignGlobal.hpp>
#include <iostream>

int main([[maybe_unused]] int argc, [[maybe_unused]] char * argv[])
{
	try
	{
		{
			std::string const sa = "MMMMMMMMMMRRMMRRMMM";
			std::string const sb = "MMMMMMMMMMMMMMM";

			libmaus2::protein::AlignGlobalBlosum62 align;
			auto const R = align.align(sa.begin(),sa.end(),sb.begin(),sb.end());
			align.print(sa.begin(),sb.begin(),R,std::cerr);
			int64_t const score = std::get<0>(R);
			std::cerr << "score=" << score << std::endl;
			assert ( score == 71 );
		}

		{
			std::string const sa = "MMMMMMMMMMRRMMRRMMMYY";
			std::string const sb = "MMMMMMMMMMMMMMM";

			libmaus2::protein::AlignGlobalBlosum62 align;
			auto const R = align.alignPrefix(sa.begin(),sa.end(),sb.begin(),sb.end());
			align.print(sa.begin(),sb.begin(),R,std::cerr);
			int64_t const score = std::get<0>(R);
			std::cerr << "score=" << score << std::endl;
			// assert ( score == 71 );
		}

		{
			std::string const sa = "AAAMMMXXX";
			std::string const sb = "XXXMMMAAA";
			std::size_t const seedposa = 3;
			std::size_t const seedposb = 3;

			libmaus2::protein::AlignGlobalBlosum62 align;
			auto const R = align.align(sa.begin(),sa.end(),seedposa,sb.begin(),sb.end(),seedposb);

			std::size_t const abpos = R.startpos_a;
			std::size_t const aepos = R.endpos_a;
			std::size_t const bbpos = R.startpos_b;
			std::size_t const bepos = R.endpos_b;

			align.print(
				sa.begin()+abpos,
				sb.begin()+bbpos,
				std::make_tuple(R.score,R.al_O),
				std::cerr
			);
		}
	}
	catch(std::exception const & ex)
	{
		std::cerr << ex.what() << std::endl;
		return EXIT_FAILURE;
	}
}
