libmaus2
========

libmaus2 is a collection of data structures and algorithms. It contains

 - I/O classes (single byte and UTF-8)
 - bitio classes (input, output and various forms of bit level manipulation)
 - text indexing classes (suffix and LCP array, fulltext and minute (FM), ...)
 - BAM sequence alignment files input/output (simple and collating)

and many lower level support classes.

-------------------------------------------------------------------------------

The main development branch of libmaus2 is hosted by gitlab at

https://gitlab.com/german.tischler/libmaus2

Release packages can be found at

https://gitlab.com/german.tischler/libmaus2/tags

Please make sure to choose a package containing the word "release" in it's name if you
intend to compile libmaus2 for production (i.e. non development) use.

-------------------------------------------------------------------------------

Compilation
-----------

libmaus2 uses the GNU autoconf/automake tool set. It can be compiled on Linux
using:

	libtoolize
	aclocal
	autoreconf -i -f
	./configure
	make

Running autoreconf requires a complete set of tools including autoconf, automake,
autoheader, aclocal and libtool.

The release packages come with a configure script, so running libtoolize, aclocal etc should not be necessary, i.e.

	./configure
	make

should be sufficient to compile libmaus2.

A full list of configuration parameters can be obtained by calling

	./configure --help

libmaus2 can use functionality from several other code bases. This includes:

 - snappy [https://github.com/google/snappy] : a fast and lightweight Lempel-Ziv
   type compression/decompression library. This can be used for the compression
   of temporary files (as used in name collating BAM input for instance).
 - io_lib [https://github.com/jkbonfield/io_lib] : This is
   part of the Staden package. libmaus2 can use this library for SAM and CRAM
   file input. The minimum version of io_lib required is 1.14.13.
 - libsecrecy [https://gitlab.com/german.tischler/libsecrecy] : a library
   for data encryption and decryption allowing random access on the read side
   The url scheme used is libsecrecy[keyname]:path, where providing a key
   name is not necessary for decryption.


